CONFIG += console
QT += core midi

SOURCES += \
    main.cpp \
    midicommunicator.cpp

HEADERS += \
    midicommunicator.h

target.path = $$[QT_INSTALL_EXAMPLES]/midi/miditerminal
INSTALLS += target
