#ifndef QWINMMMIDIPLUGIN_H
#define QWINMMMIDIPLUGIN_H


#include <QtMidi/qmidisystemplugin.h>
#include "qwinmmmidiinputbackend.h"
#include "qwinmmmidioutputbackend.h"

QT_BEGIN_NAMESPACE

class QWinMMMidiPlugin : public QMidiSystemPlugin
{
    Q_OBJECT

    Q_PLUGIN_METADATA(IID "org.qt-project.qt.midisystemfactory/5.0" FILE "winmm.json")
public:
    QWinMMMidiPlugin(QObject* parent = 0);
    virtual ~QWinMMMidiPlugin();

    QList<QByteArray> availableDevices(QMidi::Mode) const Q_DECL_OVERRIDE;
    QAbstractMidiInput* createInput(const QMidiDeviceInfo& info) Q_DECL_OVERRIDE;
    QAbstractMidiOutput* createOutput(const QMidiDeviceInfo& info) Q_DECL_OVERRIDE;
    QString deviceName(const QByteArray& device, QMidi::Mode mode) Q_DECL_OVERRIDE;

private:
    QWinMMMidiInputBackend mInputBackend;
    QWinMMMidiOutputBackend mOutputBackend;
};

QT_END_NAMESPACE

#endif // QWINMMMIDIPLUGIN_H
