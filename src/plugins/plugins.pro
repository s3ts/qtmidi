######################################################################
#
# Qt Midi
#
######################################################################

TEMPLATE = subdirs

android {
    SUBDIRS += android
}

win32:!winrt {
    SUBDIRS += winmm
}

winrt {
    SUBDIRS += winrt
}

linux:!android {
    SUBDIRS += alsa
}


mac {
    SUBDIRS += pgmidi
}

mac:!simulator {
    #SUBDIRS += coremidi
}

ios {
}


