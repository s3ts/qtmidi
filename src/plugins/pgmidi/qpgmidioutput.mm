#include "qpgmidioutput.h"
#import "qpgmidimonitor.h"
#include "qpgmidiwrapper.h"

QT_BEGIN_NAMESPACE

QPGMidiOutput::QPGMidiOutput(const QMidiDeviceInfo &info, QPGMidiWrapper &wrapper)
    : QAbstractMidiOutput(info)
    , mWrapper(wrapper)
    , mOutputDevice(nil)
{
    auto monitor = (QPGMidiMonitor*)mWrapper.monitor();
    mOutputDevice = [monitor destination:handle()];
    if (mOutputDevice != nil) {
        mState = QMidi::ConnectedState;
    }
}

QPGMidiOutput::~QPGMidiOutput() {
    mOutputDevice = nil;
}

void QPGMidiOutput::sendMidiMessage(const QMidiMessage& m) const {
    if (mOutputDevice == nil) {return;}
    auto dest = (PGMidiDestination*)mOutputDevice;
    [dest sendBytes:m.message().data() size:m.size()];
}

QT_END_NAMESPACE
