#include <QtCore/qdebug.h>

#include "qmidi.h"
#include "qmidisystemplugin.h"
#include "qmidiinput.h"

#include "qmidipluginloader_p.h"
#include "qmididevicefactory_p.h"
#include "qmidisystemnotifierprivate_p.h"

QT_BEGIN_NAMESPACE

static QString defaultKey()
{
    return QStringLiteral("default");
}

#if !defined (QT_NO_LIBRARY) && !defined(QT_NO_SETTINGS)
Q_GLOBAL_STATIC_WITH_ARGS(QMidiPluginLoader, midiLoader,
                          (QMidiSystemFactoryInterface_iid, QLatin1String("midi"), Qt::CaseInsensitive))
#endif

void QMidiDeviceFactory::instantiate() {
#if !defined (QT_NO_LIBRARY) && !defined(QT_NO_SETTINGS)
    midiLoader();
#endif
}

class QNullInputDevice : public QAbstractMidiInput
{
public:
    QNullInputDevice(const QMidiDeviceInfo &deviceInfo)
        : QAbstractMidiInput(deviceInfo)
    {}

    QMidi::Error error() const override { return QMidi::OpenError; }
    QMidi::State state() const override { return QMidi::DisconnectedState; }
};

class QNullOutputDevice : public QAbstractMidiOutput
{
public:
    QNullOutputDevice(const QMidiDeviceInfo &deviceInfo)
        : QAbstractMidiOutput(deviceInfo)
    {}

    QMidi::Error error() const override { return QMidi::OpenError; }
    QMidi::State state() const override { return QMidi::DisconnectedState; }
    void sendMidiMessage(const QMidiMessage&) const override {}
};

QMidiDeviceInfo QMidiDeviceFactory::createDeviceInfo(const QString& realm, const QByteArray& handle, QMidi::Mode mode)
{
    return QMidiDeviceInfo(realm, handle, mode);
}

QList<QByteArray> QMidiDeviceFactory::availableDeviceHandles(const QString& realm, QMidi::Mode mode)
{
    QList<QByteArray> devices;
#if !defined (QT_NO_LIBRARY) && !defined(QT_NO_SETTINGS)
    QMidiPluginLoader* l = midiLoader();
    QMidiSystemFactoryInterface* plugin = qobject_cast<QMidiSystemFactoryInterface*>(l->instance(realm));

    if (plugin) {
        return plugin->availableDevices(mode);
    }
#endif

    return QList<QByteArray>();
}

QList<QMidiDeviceInfo> QMidiDeviceFactory::availableDevices(QMidi::Mode mode)
{
    QList<QMidiDeviceInfo> devices;
#if !defined (QT_NO_LIBRARY) && !defined(QT_NO_SETTINGS)
    QMidiPluginLoader* l = midiLoader();
    const auto keys = l->keys();
    for (const QString& key : keys) {
        QMidiSystemFactoryInterface* plugin = qobject_cast<QMidiSystemFactoryInterface*>(l->instance(key));

        if (plugin) {
            const auto availableDevices = plugin->availableDevices(mode);
            for (const QByteArray& handle : availableDevices)
                devices << QMidiDeviceInfo(key, handle, mode);
        }
    }
#endif

    return devices;
}

QMidiDeviceInfo QMidiDeviceFactory::defaultInputDevice()
{
#if !defined (QT_NO_LIBRARY) && !defined(QT_NO_SETTINGS)
    QMidiSystemFactoryInterface* plugin = qobject_cast<QMidiSystemFactoryInterface*>(midiLoader()->instance(defaultKey()));
    if (plugin) {
        QList<QByteArray> list = plugin->availableDevices(QMidi::MidiInput);
        if (list.size() > 0)
            return QMidiDeviceInfo(defaultKey(), list.back(), QMidi::MidiInput);
    }

    // if no plugin is marked as default or if the default plugin doesn't have any input device,
    // return the first input available from other plugins.
    QList<QMidiDeviceInfo> inputDevices = availableDevices(QMidi::MidiInput);
    if (!inputDevices.isEmpty())
        return inputDevices.last();
#endif

    return QMidiDeviceInfo();
}

QMidiDeviceInfo QMidiDeviceFactory::defaultOutputDevice()
{
#if !defined (QT_NO_LIBRARY) && !defined(QT_NO_SETTINGS)
    QMidiSystemFactoryInterface* plugin = qobject_cast<QMidiSystemFactoryInterface*>(midiLoader()->instance(defaultKey()));
    if (plugin) {
        QList<QByteArray> list = plugin->availableDevices(QMidi::MidiOutput);
        if (list.size() > 0)
            return QMidiDeviceInfo(defaultKey(), list.at(0), QMidi::MidiOutput);
    }

    // if no plugin is marked as default or if the default plugin doesn't have any output device,
    // return the first output available from other plugins.
    QList<QMidiDeviceInfo> outputDevices = availableDevices(QMidi::MidiOutput);
    if (!outputDevices.isEmpty())
        return outputDevices.last();
#endif

    return QMidiDeviceInfo();
}

QString QMidiDeviceFactory::midiDeviceName(const QString &realm, const QByteArray &handle, QMidi::Mode mode)
{
#if !defined (QT_NO_LIBRARY) && !defined(QT_NO_SETTINGS)
    QMidiSystemFactoryInterface* plugin =
        qobject_cast<QMidiSystemFactoryInterface*>(midiLoader()->instance(realm));

    if (plugin)
        return plugin->deviceName(handle, mode);
#endif

    return QString();
}

QAbstractMidiInput* QMidiDeviceFactory::createDefaultInputDevice()
{
    return createInputDevice(defaultInputDevice());
}

QAbstractMidiOutput* QMidiDeviceFactory::createDefaultOutputDevice()
{
    return createOutputDevice(defaultOutputDevice());
}

QAbstractMidiInput* QMidiDeviceFactory::createInputDevice(QMidiDeviceInfo const& deviceInfo)
{
    if (deviceInfo.handle().isNull())
        return new QNullInputDevice(deviceInfo);

    QAbstractMidiInput* p = nullptr;
#if !defined (QT_NO_LIBRARY) && !defined(QT_NO_SETTINGS)
    QMidiSystemFactoryInterface* plugin =
        qobject_cast<QMidiSystemFactoryInterface*>(midiLoader()->instance(deviceInfo.realm()));

    if (plugin) {
        p = plugin->createInput(deviceInfo);
    }
#endif
    if (!p) {
        return new QNullInputDevice(deviceInfo);
    } else {
        return p;
    }
}

QAbstractMidiOutput* QMidiDeviceFactory::createOutputDevice(QMidiDeviceInfo const& device)
{
    if (device.handle().isNull())
        return new QNullOutputDevice(device);

    QAbstractMidiOutput* p = nullptr;
#if !defined (QT_NO_LIBRARY) && !defined(QT_NO_SETTINGS)
    QMidiSystemFactoryInterface* plugin =
        qobject_cast<QMidiSystemFactoryInterface*>(midiLoader()->instance(device.realm()));

    if (plugin) {
        p = plugin->createOutput(device);
    }
#endif

    if (p) {return p;}
    else {return new QNullOutputDevice(device);}
}

QT_END_NAMESPACE
