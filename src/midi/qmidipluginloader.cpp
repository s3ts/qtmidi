#include "qmidipluginloader_p.h"
#include <QtCore/qcoreapplication.h>
#include <QtCore/qdebug.h>
#include <QtCore/qjsonarray.h>
#include <private/qfactoryloader_p.h>

#include "qmididevicewatcher_p.h"

QMidiPluginLoader::QMidiPluginLoader(const char *iid, const QString &suffix, Qt::CaseSensitivity caseSensitivity)
    : m_iid(iid)
{
    m_location = QString::fromLatin1("/%1").arg(suffix);
    m_factoryLoader = new QFactoryLoader(m_iid, m_location, caseSensitivity);
    loadMetadata();
}

QMidiPluginLoader::~QMidiPluginLoader()
{
    delete m_factoryLoader;
}

QStringList QMidiPluginLoader::keys() const
{
    return m_metadata.keys();
}

QObject* QMidiPluginLoader::instance(QString const &key)
{
    if (!m_metadata.contains(key))
        return 0;

    int idx = m_metadata.value(key).first().value(QStringLiteral("index")).toDouble();
    if (idx < 0)
        return 0;

    return m_factoryLoader->instance(idx);
}

QList<QObject*> QMidiPluginLoader::instances(QString const &key)
{
    if (!m_metadata.contains(key))
        return QList<QObject*>();

    QList<QObject*> objects;
    const auto list = m_metadata.value(key);
    for (const QJsonObject &jsonobj : list) {
        int idx = jsonobj.value(QStringLiteral("index")).toDouble();
        if (idx < 0)
            continue;

        QObject* object = m_factoryLoader->instance(idx);
        if (!objects.contains(object)) {
            objects.append(object);
        }
    }

    return objects;
}

void QMidiPluginLoader::loadMetadata()
{
#if !defined QT_NO_DEBUG
    const bool showDebug = qgetenv("QT_DEBUG_PLUGINS").toInt() > 0;
#endif

#if !defined QT_NO_DEBUG
    if (showDebug)
        qDebug() << "QMidiPluginLoader: loading metadata for iid " << m_iid << " at location " << m_location;
#endif

    if (!m_metadata.isEmpty()) {
#if !defined QT_NO_DEBUG
        if (showDebug)
            qDebug() << "QMidiPluginLoader: already loaded metadata, returning";
#endif
        return;
    }

    bool deviceWatcher = false;

    QList<QJsonObject> meta = m_factoryLoader->metaData();
    for (int i = 0; i < meta.size(); i++) {
        QJsonObject jsonobj = meta.at(i).value(QStringLiteral("MetaData")).toObject();
#if !defined QT_NO_DEBUG
        if (showDebug)
            qDebug() << "QMidiPluginLoader: Inserted index " << i << " into metadata: " << jsonobj;
#endif

        QJsonArray arr = jsonobj.value(QStringLiteral("Services")).toArray();

        // Preserve compatibility with older plugins (made before 5.1) in which
        // services were declared in the 'Keys' property
        if (arr.isEmpty())
            arr = jsonobj.value(QStringLiteral("Keys")).toArray();

#if QT_VERSION < QT_VERSION_CHECK(5, 7, 0)
        foreach (QJsonValue value, arr) {
#else
        for (const QJsonValue &value : qAsConst(arr)) {
#endif
            QString key = value.toString();

            if (!m_metadata.contains(key)) {
#if !defined QT_NO_DEBUG
                if (showDebug)
                    qDebug() << "QMidiPluginLoader: Insterting new list for key: " << key;
#endif

                m_metadata.insert(key, QList<QJsonObject>());

                if (jsonobj.value(QStringLiteral("DeviceWatcher")).toBool()) {
                    deviceWatcher = true;
                }
            }

            m_metadata[key].append(jsonobj);
        }

    }

    if (deviceWatcher) {
        new QMidiDeviceWatcher(QMidi::MidiInput, 1000, this);
        new QMidiDeviceWatcher(QMidi::MidiOutput, 1000, this);
    }
}

QT_END_NAMESPACE
