TARGET = QtMidi
QT = core-private gui-private
qtHaveModule(androidextras):QT += androidextras

MODULE_PLUGIN_TYPES = \
    midi

QMAKE_DOCS = $$PWD/doc/qtmidi.qdocconf

INCLUDEPATH *= .

PRIVATE_HEADERS += \
    qmididevicefactory_p.h \
    qmididevicewatcher_p.h \
    qmidipluginloader_p.h \
    qmidisystemnotifierprivate_p.h \
    qmidiautoconnectorprivate_p.h

PUBLIC_HEADERS += \
    qmidi.h \
    qmidideviceinfo.h \
    qmididevice.h \
    qmidiinput.h \
    qmidimessage.h \
    qmidioutput.h \
    qmidisystem.h \
    qmidisystemnotifier.h \
    qmidisystemplugin.h \
    qtmididefs.h \
    qmidiautoconnector.h \
    qmidimessagecreator.h \

SOURCES += \
    qmidi.cpp \
    qmidisystem.cpp \
    qmidisystemplugin.cpp \
    qmidideviceinfo.cpp \
    qmididevicefactory.cpp \
    qmidipluginloader.cpp \
    qmidiinput.cpp \
    qmidisystemnotifier.cpp \
    qmidisystemnotifierprivate.cpp \
    qmididevicewatcher.cpp \
    qmidioutput.cpp \
    qmididevice.cpp \
    qmidimessage.cpp \
    qmidiautoconnector.cpp \
    qmidiautoconnectorprivate.cpp \
    qmidimessagecreator.cpp \

CONFIG += simd optimize_full

ANDROID_BUNDLED_JAR_DEPENDENCIES = \
    jar/QtMidi-bundled.jar:org.qtproject.qt5.android.midi.QAndroidMidiInterface \
    jar/QtAndroidMidiDriver-bundled.jar

ANDROID_JAR_DEPENDENCIES = \
    jar/QtMidi.jar:org.qtproject.qt5.android.midi.QAndroidMidiInterface \
    jar/QtAndroidMidiDriver.jar

ANDROID_LIB_DEPENDENCIES = \
    plugins/midi/libqtandroid_midi.so

ANDROID_PERMISSIONS +=

ANDROID_FEATURES += \
    android.hardware.usb.host

MODULE_WINRT_CAPABILITIES_DEVICE += 
    usb

HEADERS += $$PUBLIC_HEADERS $$PRIVATE_HEADERS \

load(qt_module)

DISTFILES += \
    configure.json
