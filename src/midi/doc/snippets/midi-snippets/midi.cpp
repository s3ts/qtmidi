#include <QDebug>

#include <qmidideviceinfo.h>
#include <qmidiinput.h>
#include <qmidiautoconnector.h>

class MidiInputExample : public QObject {
    Q_OBJECT
public:
};


void MidiDeviceInfo()
{
    //! [Dumping input devices]
    foreach (QMidiDeviceInfo &deviceInfo, QMidiDeviceInfo::availableDevices(QMidi::MidiInput))
        qDebug() << "Device name:" << deviceInfo.deviceName();
    //! [Dumping input devices]
}


class MidiAutoConnectorExample : public QObject {
    Q_OBJECT
public:
    void start();
};

void MidiAutoConnectorExample::start()
{
    //! [Auto connector]
    auto connector = new QMidiAutoConnector(this);
    connect(connector, &QMidiAutoConnector::deviceCreated, [](const QMidiDevice* device, QMidi::Mode mode)
    {
        qDebug() << "Automatically connected to" << mode << "with device name" << device->deviceInfo().deviceName();

        // connect to QMidiInput::notify here
    });
    connect(connector, &QMidiAutoConnector::deviceDeleted, [](const QMidiDeviceInfo info, QMidi::Mode mode)
    {
        qDebug() << "Automatically disconnected from" << mode << "with device name" << info.deviceName();
    });
    //! [Auto connector]
}
