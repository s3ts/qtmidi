TEMPLATE = subdirs

SUBDIRS += midi

#include($$OUT_PWD/midi/qtmidi-config.pri)
QT_FOR_CONFIG += midi-private

src_plugins.subdir = plugins
src_plugins.depends = midi

src_3rdparty_android-midi-driver.subdir = 3rdparty/android-midi-driver
src_3rdparty_android-midi-driver.target = android-midi-driver

src_3rdparty_pgmidi.subdir = 3rdparty/pgmidi
src_3rdparty_pgmidi.target = pgmidi

android {
    SUBDIRS += src_3rdparty_android-midi-driver
    src_plugins.depends += src_3rdparty_android-midi-driver
}

mac {
    SUBDIRS += src_3rdparty_pgmidi
    src_plugins.depends += src_3rdparty_pgmidi
}

qtHaveModule(quick) {
    # src_qtmidiquicktools.subdir = qtmidiquicktools
    # src_qtmidiquicktools.depends = midi

    src_imports.subdir = imports
    src_imports.depends = midi # src_qtmidiquicktools

    SUBDIRS += \
        src_imports \
        # src_qtmidiquicktools \
}

SUBDIRS += src_plugins
